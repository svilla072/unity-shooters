﻿using UnityEngine;
using System.Collections;

public class DestruirBala : MonoBehaviour
{
    public float vidaBala;
    public GameObject balaPrefab;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        vidaBala += 1 * Time.deltaTime;
    }
    void OnTriggerEnter2D(Collider2D c)
    {
        //if (c.CompareTag("balaPrefab") && vidaBala >= 0.5)
        if (vidaBala >= 1.0f)
        {
            Destroy(balaPrefab.gameObject);
            vidaBala = 0;
        }

    }
   

}