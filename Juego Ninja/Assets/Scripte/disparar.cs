﻿using UnityEngine;
using System.Collections;

public class disparar : MonoBehaviour {
    public float delay;
    public float speed;
    public GameObject bala;
    
    
    // Use this for initialization
    void Start () {
	
	}
	// Update is called once per frame
	void Update () 
    {
        delay += 1 * Time.deltaTime;
    }
    
    void OnTriggerStay2D(Collider2D c)
    {

        if (c.gameObject.name == "personaje" && delay >= 0.5 )
        {

            GameObject newBala = Instantiate(bala, transform.position, transform.rotation) as GameObject;

            newBala.GetComponent<Rigidbody2D>().velocity = transform.right * speed;
            
            delay = 0;

        }
    }
}