﻿using UnityEngine;
using System.Collections;

public class DobleSalto : MonoBehaviour
{

    private Rigidbody2D cuerpo;

    public float saltar;
    private int Nsaltos = 0;
    // Use this for initialization
    void Start()
    {
        cuerpo = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            Nsaltos += 1;
            if (Nsaltos == 1 || Nsaltos == 2)
            {
                cuerpo.velocity = new Vector2(cuerpo.velocity.x, saltar + Nsaltos);
            }

        }
    }
    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Plataforma")
        {
            Nsaltos = 0;
        }


    }

}
