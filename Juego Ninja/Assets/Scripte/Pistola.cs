﻿using UnityEngine;
using System.Collections;

public class Pistola : MonoBehaviour {
   
    public GameObject balaPrefab;
    public float speed;
    public float vidaBala;
    void start()
    {
        
    }
	// Update is called once per frame
	void Update () {

        vidaBala += 1 * Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.P)) 
        {    
            GameObject newBala = Instantiate(balaPrefab, transform.position, transform.rotation) as GameObject;
            newBala.GetComponent<Rigidbody2D>().velocity = transform.right * speed;   
            
 		}
              
	}
}
