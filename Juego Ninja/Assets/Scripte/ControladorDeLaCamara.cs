﻿using UnityEngine;
using System.Collections;

public class CameraTracking : MonoBehaviour
{
    // Target settings
    public Transform Target;
    private Vector2 TargetViewportPoint;

    // Camera settings
    public float UpDownSpeed = 0.1f;

    // Important variables
    private Camera cam;
    private Vector3 CameraPosition;
    private Vector2 CameraPositionViewportPoint;

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
        CameraPosition.z = gameObject.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        CameraPosition.x = Target.transform.position.x;
        TargetViewportPoint = cam.WorldToViewportPoint(Target.transform.position);


        if (TargetViewportPoint.y >= 0.75)
        {
            CameraPosition.y += UpDownSpeed;
        }
        if (TargetViewportPoint.y <= 0.30)
        {
            CameraPosition.y -= UpDownSpeed;
        }

        gameObject.transform.position = CameraPosition;
    }
}
