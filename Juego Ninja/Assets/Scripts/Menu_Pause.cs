﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu_Pause : MonoBehaviour {

	public GameObject pause;
	public GameObject pauseBrillo;

	// Use this for initialization
	void Start ()
	{
		pauseBrillo.SetActive (false);
		pause.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reanudar()
	{
		
	}

	public void Opciones()
	{
		pause.SetActive (false);
		pauseBrillo.SetActive (true);
	}

	public void Salir()
	{
		SceneManager.LoadScene ("Menu");
	}

	public void Atras()
	{
		pauseBrillo.SetActive (false);
		pause.SetActive (true);
	}
}