﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public GameObject menu;
	public GameObject opciones;
	public GameObject brillo;

	// Use this for initialization
	void Start () 
	{
		menu.SetActive (true);
		opciones.SetActive (false);
		brillo.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void Jugar()
	{
		SceneManager.LoadScene("Juego Ninja");
	}

	public void Opciones()
	{
		menu.SetActive (false);
		opciones.SetActive (true);
	}

	public void Salir()
	{
		Application.Quit();
		Debug.Log("EXIT");
	}

	public void Atras()
	{
		opciones.SetActive (false);
		menu.SetActive (true);
	}

	public void Brillo()
	{
		opciones.SetActive (false);
		brillo.SetActive (true);
	}

	public void Atras_Brillo()
	{
		brillo.SetActive (false);
		opciones.SetActive (true);
	}
}