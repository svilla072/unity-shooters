﻿using UnityEngine;
using System.Collections;

public class Brillo : MonoBehaviour {

	public float alpha = Variables_Guardadas.brillo;

	void Start () 
	{
		alpha = Variables_Guardadas.brillo;
	}

	void Update ()
	{
		GetComponent<SpriteRenderer> ().color = new Color (1,1,1,alpha);
	}

	public void AdjustableAlpha (float newAlpha)
	{
		alpha = newAlpha;
		Variables_Guardadas.brillo = newAlpha;
	}
}